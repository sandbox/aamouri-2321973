# Description

Useful Commands Drush provides a list of useful Drush commands for developers.

# Installation

1. Extract archive with module to contributed modules directory.
2. Enable it on Modules page.

# Features

More information about features can be found on project page
http://drupal.org/project/useful_commands_drush

# Issues

If you experience any problems with SecKit, please,
report issues on http://drupal.org/project/issues/useful_commands_drush

# Author

Akram AMOURI
https://github.com/aamouri
