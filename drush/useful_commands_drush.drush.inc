<?php
/**
 * @file
 * Drush commands for useful commands drush.
 */

/**
 * Implements hook_drush_command().
 */
function useful_commands_drush_drush_command() {
  $items = array();

  $items['remove-nodes-by-type'] = array(
    'callback' => 'useful_commands_drush_clean_nodes_by_type',
    'description' => dt('Remove nodes by type.'),
    'aliases' => array('rnbt'),
    'examples' => array(
      'drush remove-nodes-by-type article' => 'Remove all nodes of article type.',
      'drush remove-nodes-by-type page' => 'Remove all nodes of page type',
    ),
    'arguments' => array(
      'type' => "Required. The type of nodes to remove",
    ),
  );

  $items['clean-system'] = array(
    'callback' => 'useful_commands_drush_clean_system',
    'description' => dt('Find and remove enabled modules that do not exist from the database.'),
    'examples' => array('drush clean-system'),
    'aliases' => array('csy'),
  );

  $items['print-db-setting'] = array(
    'callback' => 'useful_commands_drush_print_db_setting',
    'description' => 'Prints the db connection string from settings.php',
    'arguments' => array(),
    'examples' => array('drush print-db-setting'),
    'aliases' => array('pdbs'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_CONFIGURATION,
  );

  $items['drush-revert-views'] = array(
    'callback' => 'useful_commands_drush_revert_views',
    'drupal dependencies' => array('views'),
    'description' => 'Revert overridden views to their default state. Make sure to backup first.',
    'arguments' => array(),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'aliases' => array('drv'),
    'examples' => array(
      'drush drush-revert-views' => 'Reverts all exported views!',
    ),
  );

  return $items;
}


/**
 * Implements useful_commands_drush_drush_help().
 */
function useful_commands_drush_drush_help($section) {
  switch ($section) {
    case 'drush:remove-nodes-by-type':
      return dt("Remove all contents of a specific type.");

    case 'drush:clean-system':
      return dt("Cleanup all needless modules in the system.");

    case 'drush:print-db-setting':
      return dt("This command simply prints the DB connection string from settings.php");

    case 'drush:drush-revert-views':
      return dt("This command reverts ALL overridden views!");

  }
}

/**
 * Print the setting of Data base connection.
 */
function useful_commands_drush_print_db_setting() {
  if (drush_drupal_version() >= 7) {
    global $databases;
    $db = $databases['default']['default'];
    print $db['driver'] . '://' . $db['username'] . ':' . $db['password'] . '@' . $db['host'] . '/' . $db['database'] . "\n";
  }
  else {
    global $db_url;
    print $db_url . "\n";
  }
}


/**
 * Revert all views exported.
 */
function useful_commands_drush_revert_views() {
  $i = views_revert_allviews(views_get_all_views());
  drush_log(dt('Reverted a total of @count views.', array('@count' => $i)), 'ok');
}


/**
 * Clean nodes by type.
 */
function useful_commands_drush_clean_nodes_by_type() {

  $args = func_get_args();
  if (count($args) > 0) {
    foreach ($args as $type) {

      $exist = useful_commands_drush_get_type($type);
      if (!$exist) {
        _useful_commands_drush_message("Could not find the type '@type'.", array('@type' => $type), 'error');
        return;
      }

      _useful_commands_drush_message('Starting remove nodes ...');
      useful_commands_drush_remove_nodes($type);

    }
  }

}


/**
 * Find and remove missing modules from the database.
 */
function useful_commands_drush_clean_system() {
  $modules = db_query("SELECT name, filename, info FROM {system} WHERE type = 'module' AND status = 1 ORDER BY filename");

  $to_disable = array();

  _useful_commands_drush_message('Starting checking all modules ...');
  timer_start('useful_commands_drush');

  foreach ($modules as $module) {
    $info = unserialize($module->info);
    $filepath = DRUPAL_ROOT . '/' . $module->filename;
    if (!file_exists($filepath)) {
      drush_print($info['name']);
      $to_disable[] = $module->name;
    }

  }
  if ($to_disable) {
    if (!drush_confirm(dt('Are you sure you want to disable the modules above?'))) {
      return drush_user_abort();
    }
    $to_array = array();
    $disabling = 0;
    foreach ($to_disable as $to) {
      $disabling++;
      _drush_print_progress($disabling / count($to_disable));
      $to_array[] = $to;
    }
    db_update('system')
      ->fields(array('status' => 0))
      ->condition('name', $to_disable)
      ->condition('type', 'module')
      ->execute();
    unset($to_array);
  }
  else {
    drush_print(dt('Nothing to disable.'));
  }

  // Cleanup succeeded,
  $time = timer_stop('useful_commands_drush');
  $message = 'The system are cleanup in !time ms.';
  $params = array(
    '!time' => $time['time'],
  );

  useful_commands_drush_succeed($message, $params);

}


/**
 * Remove all nodes of a specific type.
 */
function useful_commands_drush_remove_nodes($type) {

  timer_start('useful_commands_drush');

  $ids = node_load_multiple(array(), array('type' => $type));
  $ids = array_keys($ids);
  $deleted = 0;
  foreach ($ids as $nid) {
    node_delete($nid);
    $deleted++;
    _drush_print_progress($deleted / count($ids));
  }

  // Cleanup succeeded,
  $time = timer_stop('useful_commands_drush');
  $message = 'The nodes of %type type has been removed in !time ms.';
  $params = array(
    '%type' => $type,
    '!time' => $time['time'],
  );

  useful_commands_drush_succeed($message, $params);
  return TRUE;
}


/**
 * Provides progress bar.
 */
function _drush_print_progress($ratio) {
  $percentage = floor($ratio * 100) . '%';
  $columns = drush_get_context('DRUSH_COLUMNS', 80);
  // Subtract 8 characters for the percentage, brackets, spaces and arrow.
  $progress_columns = $columns - 8;
  // If ratio is 1 (complete), the > becomes a = to make a full bar.
  $arrow = ($ratio < 1) ? '>' : '=';
  // Print a new line if ratio is 1 (complete). Otherwise, use a CR.
  $line_ending = ($ratio < 1) ? "\r" : "\n";

  // Determine the current length of the progress string.
  $current_length = floor($ratio * $progress_columns);
  $progress_string = str_pad('', $current_length, '=');

  $output = '[';
  $output .= $progress_string . $arrow;
  $output .= str_pad('', $progress_columns - $current_length);
  $output .= ']';
  $output .= str_pad('', 5 - strlen($percentage)) . $percentage;
  $output .= $line_ending;

  print $output;
}


/**
 * Show success message Drush.
 */
function useful_commands_drush_succeed($message, $params) {
  _useful_commands_drush_message($message, $params, 'success');
  return FALSE;
}

/**
 * Get all content types.
 */
function useful_commands_drush_get_type($type) {
  $types = node_type_get_types();
  return @$types[$type];
}


/**
 * Set a drupal msg depending on whether module is being run interactively.
 */
function _useful_commands_drush_message($message, $replace = array(), $type = 'status') {
  // Only set a message if there is a callback handler to handle the message.
  if (($callback = _useful_commands_drush_message_callback()) && function_exists($callback)) {
    $callback($message, $replace, $type);
  }

  _useful_commands_drush_messages($message, $replace, $type);
}

/**
 * Set a drupal msg depending on whether module is being run interactively.
 */
function _useful_commands_drush_messages($message = NULL, $replace = array(), $type = 'status') {
  static $messages = array();
  if ($message) {
    $messages[] = array(
      'message' => $message,
      'replace' => $replace,
      'type' => 'status',
    );
  }
  return $messages;
}


/**
 * Set or retrieve a message handler.
 */
function _useful_commands_drush_message_callback($callback = NULL) {
  static $current_callback = '_useful_commands_drush_message_log';
  if ($callback !== NULL) {
    $current_callback = $callback;
  }
  return $current_callback;
}


/**
 * Send a msg to the browser. The normal type of msg handling for use.
 */
function _useful_commands_drush_message_browser($message, $replace, $type) {
  // Log the message as well for admins.
  _useful_commands_drush_message_log($message, $replace, $type);

  // If there are links, we can display them in the browser.
  if (!empty($replace['!links'])) {
    $message .= " (!links)";
  }
  // Run t function.
  // @ignore sniffer_semantics_functioncall_notliteralstring
  $text = t($message, $replace);
  $text = check_plain($text);
  // Use drupal_set_message to display to the user.
  drupal_set_message($text, str_replace('success', 'status', $type), FALSE);
}

/**
 * Log message if we are in a non-interactive mode such as a cron run.
 */
function _useful_commands_drush_message_log($message, $replace, $type) {
  // We only want to log the errors or successful completions.
  if (in_array($type, array('error', 'success'))) {
    watchdog('useful_commands_drush', $message, $replace, $type == 'error' ? WATCHDOG_ERROR : WATCHDOG_NOTICE);
  }
}
